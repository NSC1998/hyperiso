#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/

useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/zsh hyperbola

chmod 750 /etc/sudoers.d
chmod 440 /etc/sudoers.d/g_wheel

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

rc-update add gnupg-mount default
rc-update add pacman-init default
rc-update add choose-mirror default
rc-update add dhcpcd default

sed -i "s/_GALAXY_VERSION_/${galaxy_version}/" /etc/motd
sed -i "s/_ISO_VERSION_/${iso_version}/" /etc/motd
