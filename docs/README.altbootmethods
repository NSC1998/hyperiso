INDEX
-----

* Alternative boot methods (configs/releng)
  * ISO in loopback mode
  * ISO in memdisk mode
  * Network booting (PXE) [first stage]
    * DHCP + TFTP
    * DHCP + HTTP
    * HTTP/NFS/NBD [second stage]



*** Alternative boot methods (configs/releng)

ISO images names consist of: hyperbola-<YYYY>.<MM>.<DD>-dual.iso

Where:
    <YYYY> Year
    <MM> Month
    <DD> Day


** ISO in loopback mode.

Note: Described method is for using with GRUB2.
      GRUB2 is installed on target media and hyperbola-<YYYY>.<MM>.<DD>-dual.iso
      is at path <TARGET-PATH> on disk <D> and partition <P>,
      where filesystem is labeled as <TARGET-FS-LABEL>.

menuentry "Hyperbola GNU/Linux-libre (x86_64)" {
    set isofile="/<TARGET-PATH>/hyperbola-<YYYY>.<MM>.<DD>-dual.iso"
    loopback loop (hd<D>,<P>)$isofile
    linux (loop)/hyperbola/boot/x86_64/vmlinuz hyperisolabel=<FS-LABEL> img_label=<TARGET-FS-LABEL> img_loop=$isofile
    initrd (loop)/hyperbola/boot/x86_64/hyperiso.img
}

menuentry "Hyperbola GNU/Linux-libre (i686)" {
    set isofile="/<TARGET-PATH>/hyperbola-<YYYY>.<MM>.<DD>-dual.iso"
    loopback loop (hd<D>,<P>)$isofile
    linux (loop)/hyperbola/boot/i686/vmlinuz hyperisolabel=<FS-LABEL> img_label=<TARGET-FS-LABEL> img_loop=$isofile
    initrd (loop)/hyperbola/boot/i686/hyperiso.img
}


** ISO in memdisk mode.

Note: Described method is for using with SYSLINUX. Anyway MEMDISK from SYSLINUX can work
      with other bootloaders.
      SYSLINUX is installed on target media and hyperbola-<YYYY>.<MM>.<DD>-dual.iso
      is at path <TARGET-PATH>.
      On 32-bit systems, is needed to pass vmalloc=nnM to the kernel, where nn is the size
      of the ISO image plus 64 MiB (or 128 MiB).


LABEL hyperbola_x64
   LINUX memdisk
   INITRD /<TARGET-PATH>/hyperbola-<YYYY>.<MM>.<DD>-dual.iso
   APPEND iso

LABEL hyperbola_x32
   LINUX memdisk
   INITRD /<TARGET-PATH>/hyperbola-<YYYY>.<MM>.<DD>-dual.iso
   APPEND iso


** Network booting (PXE).

All ISOs are ready to act as PXE server, some manual steps are needed
to setup the desired PXE boot mode.
Alternatively it is possible to use an existing PXE server following the same logic.
Note: Setup network first, adjust IP adresses, and respect all slashes "/".

First stage is for loading kernel and initramfs via PXE, two methods described here:

* DHCP + TFTP

Note: All NIC firmwares should support this.

# dnsmasq --port=0 \
          --enable-tftp \
          --tftp-root=/run/hyperiso/bootmnt \
          --dhcp-range=192.168.0.2,192.168.0.254,86400 \
          --dhcp-boot=/hyperbola/boot/syslinux/gpxelinux.0 \
          --dhcp-option-force=209,boot/syslinux/hyperiso.cfg \
          --dhcp-option-force=210,/hyperbola/

* DHCP + HTTP

Note: Not all NIC firmware supports HTTP and DNS (if domain name is used).
      At least this works with iPXE and gPXE.

# dnsmasq --port=0 \
          --dhcp-range=192.168.0.2,192.168.0.254,86400 \
          --dhcp-boot=http://192.168.0.7/hyperbola/boot/syslinux/gpxelinux.0 \
          --dhcp-option-force=209,boot/syslinux/hyperiso.cfg \
          --dhcp-option-force=210,http://192.168.0.7/hyperbola/


Once the kernel is started from PXE, SquashFS files and other misc files
inside "hyperbola" directory must be loaded (second stage). One of the following
methods can be used to serve the rest of live-medium.

* HTTP

# darkhttpd /run/hyperiso/bootmnt


* NFS

# echo "/run/hyperiso/bootmnt 192.168.0.*(ro,no_subtree_check,no_root_squash)" >> /etc/exports
# systemctl start rpc-mountd.service


* NBD

Note: Adjust HYPER_201302 as needed.

# cat << EOF > /tmp/nbd-server.conf
[generic]
[hyperiso]
    readonly = true
    exportname = /dev/disk/by-label/PARA_201302
EOF
# nbd-server -C /tmp/nbd-server.conf
